import express from 'express';
import {getEntityFunction} from '@themost/express/middleware';
import Instructor from "../models/instructor-model";
import {HttpNotFoundError} from "@themost/common";
let router = express.Router();

router.get('/me/classes/:courseClass/:courseClassFunction', (req, res, next) => {
    // get instructor course class
    Instructor.getMe(req.context).then(q => {
        // get instructor object
        return q.select('id').getTypedItem().then(
            /**
             * @param {Instructor} instructor
             */
            instructor =>  {
                // check if instructor is undefined
                if (typeof instructor === 'undefined') {
                    // and continue
                    return next(new HttpNotFoundError());
                }
                return instructor.getInstructorClasses().where('id').equal(req.params.courseClass).select('id').getTypedItem().then(
                    /**
                     * @param {CourseClass} courseClass
                     */
                    courseClass=> {
                        // check if course class is undefined
                        if (typeof courseClass === 'undefined') {
                            // and continue
                            return next(new HttpNotFoundError());
                        }
                        // call middleware
                        return getEntityFunction({
                            entityFunctionFrom: 'courseClassFunction',
                            from: 'courseClass',
                            entitySet: 'CourseClasses'
                        })(req, res, next);
                    });
            }).catch(reason => {
            return next(reason);
        });
    });
});

router.get('/me/exams/:courseExam/:courseExamFunction', (req, res, next) => {
    // get instructor course class
    Instructor.getMe(req.context).then(q => {
        // get instructor object
        return q.select('id').getTypedItem().then(
            /**
             * @param {Instructor} instructor
             */
            instructor =>  {
                // check if instructor is undefined
                if (typeof instructor === 'undefined') {
                    // and continue
                    return next(new HttpNotFoundError());
                }
                return instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem().then(
                    /**
                     * @param {CourseExam} courseExam
                     */
                    courseExam=> {
                        // check if course exam is undefined
                        if (typeof courseExam === 'undefined') {
                            // and continue
                            return next(new HttpNotFoundError());
                        }
                        // call middleware
                        return getEntityFunction({
                            entityFunctionFrom: 'courseExamFunction',
                            from: 'courseExam',
                            entitySet: 'CourseExams'
                        })(req, res, next);
                    });
            }).catch(reason => {
            return next(reason);
        });
    });
});

module.exports = router;