import express from 'express';
import {SwaggerService} from "../services/swagger-service";
import swaggerUi from 'swagger-ui-express';
let router = express.Router();

/* GET home page. */
router.get('/schema', (req, res, next) => {
    // noinspection JSValidateTypes
    /**
     * @type {SwaggerService}
     */
    let swaggerService = req.context.getApplication().getStrategy(SwaggerService);
    if (typeof swaggerService === 'undefined') {
        return next(new Error('Invalid configuration. Swagger service cannot be found or is inaccessible.'));
    }
    return res.json(swaggerService.getSwaggerDocument(req.context));
});

let options = {
    swaggerUrl: 'schema'
};

router.use('/', swaggerUi.serve, swaggerUi.setup(null, options));

module.exports = router;