import {HttpForbiddenError, HttpServerError} from "@themost/common/errors";
import _ from 'lodash';
import {TraceUtils} from '@themost/common/utils';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        const target = event.target, context = event.model.context;
        if (event.state === 1) {
            const registrationPeriod = context.model('AcademicPeriod').convert(target.registrationPeriod).getId();
            const registrationYear =  target.registrationYear, student =  context.model('Student').convert(target.student).getId();
            const registrationModel=context.model("StudentPeriodRegistration");
            //check if it is current
            let registration = {student: student, registrationYear: registrationYear, registrationPeriod: registrationPeriod, action: event.target};
            registration = registrationModel.convert(registration);
            registration.is(':current').then(function (result) {
                if (result) {
                    registrationModel.where('student').equal(student).and('registrationYear').equal(registrationYear).and('registrationPeriod').equal(registrationPeriod).first(function(err,result){
                        if (err)
                            return callback(err);
                        if (_.isNil(result)) {
                            //do save registration
                            registration.save(context, function (err) {
                                if (err) {
                                    return callback(err);
                                }
                                else {
                                    if (registration.validationResult && (registration.validationResult.success === false)) {
                                        return callback(registration.validationResult);
                                    }
                                }
                                callback();
                            });
                        }
                        else {
                            callback();
                        }
                    });
                }
                else {
                    callback(new HttpForbiddenError('Invalid registration data. The specified registration is not referred to current academic year and period.'));
                }
            }, function (err) {
                TraceUtils.error(err);
                callback(new HttpServerError());
            });


        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}