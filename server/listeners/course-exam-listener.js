import util from 'util';
import {TraceUtils} from '@themost/common/utils';
import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        if (event.state !== 2) return callback();
        const context = event.model.context,
              /**
               * @type {{CourseExam:string|*}}
               */
              target = event.target,
              courseExamModel = context.model('CourseExam');
        const courseExam = courseExamModel.convert(target);
        courseExam.validateState(function (err) {
            if (err) {
                return callback(err);
            }
            else {
                const status = context.model('CourseExamStatus').convert(target.status).getId();
                if (status===16)
                {
                    target.status=11;
                    event.pendingDocument=true;
                }
                target.completedByUser = courseExam.completedByUser;
                target.dateCompleted = courseExam.dateCompleted;
                courseExamModel.where("id").equal(target.id).flatten().first(function (err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        event.previous = result;
                        callback();
                    }
                });
            }
        });
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        if (event.state !== 2) {
            return callback();
        }
        const context = event.model.context, target = event.target;

        const status = context.model('CourseExamStatus').convert(target.status).getId();
        let eventTitle = '';
        const prevStatus = event.previous["status"] || 0;
        //add to event but first get courseexam from db
        const courseExamModel=context.model('CourseExam');
        courseExamModel.where('id').equal(target.id).select([ "id", "year","name", "examPeriod/name as examPeriod","course/displayCode as displayCode","course/name as courseName"]).flatten().silent().first(function (err, exam) {
            if (err) {
                return callback(err);
            }
            if (!_.isNil(exam)) {
                if (status === 11 && prevStatus !== 11) {
                    eventTitle = util.format('Επεξεργασία βαθμολογίου της εξεταστικής περιόδου [%s] %s %s-%s %s του μαθήματος [%s] %s'
                        , exam.id, exam.name, exam.year, exam.year + 1, exam.examPeriod, exam.displayCode, exam.courseName);
                    context.unattended(function (cb) {
                        context.model('EventLog').save({title: eventTitle, eventType: 2, username: context.interactiveUser.name, eventSource: 'teachers', eventApplication: 'universis'}, function (err) {
                            cb(err);
                        });
                    }, function (err) {
                        if (err) {
                            TraceUtils.error(err);
                        }
                        callback();
                    });
                }
                else if (event.pendingDocument === true) {
                    const courseExam = courseExamModel.convert(exam);
                    courseExam.createDocument(function (err, docId) {
                        target.docId = docId;
                        return callback(err);
                    });
                }
                else {
                    if (status === 12 && prevStatus === 11) {
                        //check if document has been created for this user
                        callback();
                    }
                    else {
                        callback();
                    }
                }
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e);
    }
}