import {ValidationResult} from "../errors";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        const context = event.model.context,
              /**
               * @type {{courseClass:string|*}}
               */
              target = event.target,
              courseClasses = context.model('CourseClass'),
              courses = context.model('Course');
        if (event.state===1) {
            if (target.courseClass) {
                /**
                 * @type {DataObject|CourseClassModel|*}
                 */
                const courseClass = courseClasses.convert(target['courseClass']);
                courseClass.courseOf(function(err, result) {
                    if (err) { return callback(err); }
                    courses.convert(result).passed(target['student'], function(err, passed) {
                        if (err) { return callback(err); }
                        if (passed) {
                            return callback(new ValidationResult(false, 'EPASS', context.__("The specified course (or at least one of the courses replaced by this course) has been already passed.")));
                        }
                        callback();
                    });
                });
            }
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}