/**
 * @license
 * Universis Project Version 1.0
 * Copyright (c) 2018, Universis Project All rights reserved
 *
 * Use of this source code is governed by an LGPL 3.0 license that can be
 * found in the LICENSE file at https://universis.io/license
 */

import {ConfigurationBase, ConfigurationStrategy} from "@themost/common";

/**
 * Declares a configuration element for managing scope-based permissions on server resources
 */
declare interface ScopeAccessConfigurationElement {
    /**
     * Gets or sets an array of strings that holds an array of scopes e.g. students or students:read or students,teachers etc
     */
    scope: Array<string>,
    /**
     * Gets or sets a string which represents the regular expression that is going to used to validate endpoint
     */
    resource: string;
    /**
     * Gets or sets an array of strings which represents the access levels for the given scopes e.g. READ or READ,WRITE etc
     */
    access: Array<string>;
    /**
     * Gets or sets a string which represents a short description for this item
     */
    description?: string;
}



export declare class ScopeAccessConfiguration extends ConfigurationStrategy {

    constructor(configuration: ConfigurationBase);

    /**
     * Gets an array of scope access configuration elements
     */
    public elements: Array<ScopeAccessConfigurationElement>;
}

export declare class DefaultScopeAccessConfiguration extends ScopeAccessConfiguration {

    constructor(configuration: ConfigurationBase);
    /**
     * Gets an array of scope access configuration elements
     */
    public elements: Array<ScopeAccessConfigurationElement>;

}