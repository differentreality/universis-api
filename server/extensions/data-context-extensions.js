import {DefaultDataContext} from "@themost/data";
import i18n from "i18n";

/**
 * @name DefaultDataContext~__
 * @param {...*} any
 * @returns {string}
 */

/**
 * Implements i18n.__ method to DefaultDataContext
 * @returns {string}
 */
DefaultDataContext.prototype.__  = function() {
    return i18n.__.apply(this, arguments);
};

/**
 * @name DefaultDataContext~__n
 * @param {...*} any
 * @returns {string}
 */

/**
 * Implements i18n.__n method to DefaultDataContext
 * @returns {string}
 */
DefaultDataContext.prototype.__n  = function() {
    return i18n.__n.apply(this, arguments);
};