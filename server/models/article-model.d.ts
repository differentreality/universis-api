import {EdmMapping,EdmType} from '@themost/data/odata';
import Thing = require('./thing-model');
import Workspace = require('./workspace-model');

/**
 * @class
 */
declare class Article extends Thing {

     
     public id: number; 
     
     /**
      * @description The actual body of the article.
      */
     public articleBody?: string; 
     
     /**
      * @description Articles may belong to one or more 'sections' in a magazine or newspaper, such as Sports, Lifestyle, etc.
      */
     public articleSection?: string; 
     
     /**
      * @description The number of words in the text of the Article.
      */
     public wordCount?: number; 
     
     /**
      * @description The workspace the article refers to.
      */
     public workspace?: Workspace|any; 
     
     /**
      * @description The keywords/tags used to describe this content.
      */
     public keywords?: string; 

}

export = Article;