import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Student = require('./student-model');
import AcademicPeriod = require('./academic-period-model');

/**
 * @class
 */
declare class StudentRegisterAction extends DataObject {

     
     public id: number; 
     
     /**
      * @description Κωδικός φοιτητή
      */
     public student: Student|any; 
     
     /**
      * @description Ακαδημαϊκό έτος εγγραφής
      */
     public registrationYear: number; 
     
     /**
      * @description Περίοδος εγγραφής
      */
     public registrationPeriod: AcademicPeriod|any; 
     
     /**
      * @description Ημερομηνία εγγραφής
      */
     public registrationDate?: Date; 

}

export = StudentRegisterAction;