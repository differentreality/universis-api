import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from "@themost/data/data-object";
@EdmMapping.entityType('Instructor')
/**
 * @class
 * @augments DataObject
 */
class Instructor extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func("Me","Instructor")
    static getMe(context) {
        const model = context.model('Instructor');
        return new Promise(((resolve, reject) => {
            model.filter("id eq instructor()", (err, q) => {
                if (err) {
                    return reject(err);
                }
                return resolve(q);
            });
        }));
    }

    @EdmMapping.func("classes",EdmType.CollectionOf("CourseClass"))
    getInstructorClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentClasses",EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .and('period').equal('$it/course/department/currentPeriod')
            .prepare();
    }

    @EdmMapping.func("exams",EdmType.CollectionOf("CourseExam"))
    getInstructorExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentExams",EdmType.CollectionOf("CourseExam"))
    getCurrentExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .prepare();
    }

}

module.exports = Instructor;