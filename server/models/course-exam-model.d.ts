import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Course = require('./course-model');
import AcademicYear = require('./academic-year-model');
import ExamPeriod = require('./exam-period-model');
import CourseExamStatus = require('./course-exam-status-model');
import User = require('./user-model');
import GradeScale = require('./grade-scale-model');
import CourseClass = require('./course-class-model');
import CourseExamInstructor = require('./course-exam-instructor-model');

/**
 * @class
 */
declare class CourseExam extends DataObject {

     
     /**
      * @description Ο κωδικός της εξέτασης
      */
     public id: number; 
     
     /**
      * @description Το μάθημα στο οποίο αναφέρεται η εξέταση.
      */
     public course: Course|any; 
     
     /**
      * @description Το ακαδημαϊκό έτος της εξέτασης
      */
     public year: AcademicYear|any; 
     
     /**
      * @description Η εξεταστική περίοδος της εξέτασης
      */
     public examPeriod: ExamPeriod|any; 
     
     /**
      * @description Περιγραφή της εξέτασης
      */
     public description?: string; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Αν είναι επαναληπτική η εξέταση (ΝΑΙ/ΟΧΙ)
      */
     public isLate?: boolean; 
     
     /**
      * @description Η ημερομηνία διεξαγωγής της εξέτασης
      */
     public examDate?: Date; 
     
     /**
      * @description Η κατάσταση της εξέτασης.
      */
     public status: CourseExamStatus|any; 
     
     /**
      * @description Αν ο τελικός βαθμός της εξέτασης προκύπτει από επιμέρους βαθμούς (πχ πρόοδοι κλπ)
      */
     public isCalculated?: boolean; 
     
     /**
      * @description Αριθμός δεκαδικών ψηφίων του βαθμού.
      */
     public decimalDigits?: number; 
     
     /**
      * @description Η ονομασία της εξέτασης του μαθήματος (μπορεί να διαφέρει ανά έτος).
      */
     public name?: string; 
     
     /**
      * @description Η ημερομηνία ανακοίνωσης των αποτελεσμάτων.
      */
     public resultDate?: Date; 
     
     /**
      * @description Ο αριθμός των φοιτητών που προσήλθαν στην εξέταση και βαθμολογήθηκαν.
      */
     public numberOfGradedStudents?: number; 
     
     /**
      * @description Ο χρήστης που ολοκλήρωσε την εξέταση.
      */
     public completedByUser?: User|any; 
     
     /**
      * @description Η ημερομηνία ολοκλήρωσης της κατάθεσης της βαθμολογίας της εξέτασης.
      */
     public dateCompleted?: Date; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified?: Date; 
     
     /**
      * @description Η κλίμακα βαθμολογίας της εξέτασης.
      */
     public gradeScale?: GradeScale|any; 
     
     /**
      * @description Το σύνολο των τάξεων που μετέχουν στην εξέταση (πχ σε εξέταση Σεπτεμβρίου μπορεί να μετέχει και η χειμερινή αλλά και η εαρινή τάξη του μαθήματος).
      */
     public classes?: Array<CourseClass|any>; 
     
     /**
      * @description Το σύνολο των εξεταστών της εξέτασης.
      */
     public instructors?: Array<CourseExamInstructor|any>; 

}

export = CourseExam;