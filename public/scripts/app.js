/* eslint-disable no-var */
var app = angular.module("docs", ["ui.router"]);

app.controller('SidebarController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/').then(function (response) {
            vm.endpoints = response.data.value;
        });
    }
    activate();
});

app.controller('EntityTypesController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/$metadata').then(function (response) {
            // get entity types
            var xml = jQuery(response.data);
            vm.entityTypes = xml.find('EntityType').toArray().map(function(entityType) {
                return entityType.getAttribute('name');
            });
        });
    }
    activate();
});

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state({
        name: 'index',
        url: '/',
        templateUrl: 'components/index.component.html'
    }).state({
        name: 'entityTypes',
        url: '/entityTypes',
        templateUrl: 'components/entity-types.component.html'
    });

    $urlRouterProvider.otherwise('/');
});
